let express = require('express');
let router = express.Router();
let Catalog = require('../models/FlowerCatalog')


router.get('/catalog', async function (req, res) {
    let catalog = await Catalog.find({valid: true}).exec();
    //setTimeout(function () {
    res.render('pages/catalog', { catalog: catalog });
    //}, 1000);
});

router.get('/contact', function (req, res) {
    //setTimeout(function () {
    res.render('pages/contact');
    //}, 1000);
});

router.get('/about', async function (req, res) {
    let branches = await Branch.find({valid: true}).exec();
    //setTimeout(function () {
    res.render('partials/aboutBody', { branches: branches });
    //}, 1000);
});

module.exports = router;
