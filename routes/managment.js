let express = require('express');
let router = express.Router();
let Customer = require('../models/Customer');
let Employee = require('../models/Employee');
let Managment = require('../models/Managment');
let Branch = require('../models/Branch');
let Person = require('../models/Person');
let Catalog = require('../models/FlowerCatalog');

router.get('/branch',async function (req, res) {
    let branches = await Branch.find({valid: true}).exec();
    //setTimeout(function () {
    res.render('pages/branchMng', { branches: branches, UserType: 'managment' });
    //}, 1000);
});

router.get('/user', async function (req, res) {
    let customers = await Customer.find().populate('details').exec();
    let employees = await Employee.find().populate('details').exec();
    let managments = await Managment.find().populate('details').exec();
    res.render('pages/userMng', { customers: customers, employees: employees, managments: managments, UserType: 'managment' });
});

router.get('/userEmp',async function (req, res) {
    let customers = await Customer.find().populate('details').exec();
    //setTimeout(function () {
    res.render('pages/userMng', { customers: customers, UserType: 'employee' });
    //}, 1000);
});

router.get('/catalog', async function (req, res) {
    let catalog = await Catalog.find().exec();
    //setTimeout(function () {
    res.render('pages/catalogMng', { catalog: catalog });
    //}, 1000);
});

module.exports = router;
