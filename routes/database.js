let express = require('express');
let router = express.Router();
let Person = require('../models/Person');
let mongoose = require('mongoose');
let Managment = require('../models/Managment');
let Employee = require('../models/Employee');
let Customer = require('../models/Customer');
let Branch = require('../models/Branch');
let Flower = require('../models/FlowerCatalog');
let multer = require('multer');

let counter = 1;
let currentImgName = '';

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        console.log('new folder');
        cb(null, '../public/images/uploads')
    },
    filename: function (req, file, cb) {
        // currentImgName = 'flower' + counter + '.jpg';
        cb(null, file.fieldname + counter);
        counter++;
    }
});

let upload = multer({ storage: storage });
// let upload = multer({dest:'uploads/'});
// var app=express();



router.post('/addFlower', upload.single('image') , (req, res,next) => {
    let flower = new Flower({
        name: req.body['flrName'],
        color: req.body['flrColor'],
        //image: "images/uploads/" + currentImgName,
        //image: "images/uploads/flower.jpg",
        image: req.body['flrImg'],
        // image: req.files,
        price: req.body['flrPrice'],
        valid: true
    });
    flower.save();
    //setTimeout(function () {
    res.end();
    //}, 1000);
});

router.post('/updateUser', async function (req, res) {
    let futurePermission = req.body['permission'];
    let userName = req.body['userName'];
    let permittedUser = await Person.findOne({ userName: userName }).exec();
    if (permittedUser) {
        console.log('person before---------- \n' + await Person.find().exec());
        let permission = permittedUser.permission;
        console.log(futurePermission); 
        if (permission == futurePermission)
            res.end();
        else
        {
            if (permission == 'managment') {
                let mngList = await Managment.find().populate({
                    path: 'details',
                    match: {
                        userName: userName
                    }
                }).exec();
                let i;
                for (i = 0; i < mngList.length; i++)
                    if (mngList[i].details !== null)
                    {
                        await Managment.remove({ _id: mngList[i]._id }).exec();
                        await Person.remove({ _id: mngList[i].details._id }).exec();
                        break;
                    }
            }
            else if (permission == 'employee') {
                let empList = await Employee.find().populate({
                    path: 'details',
                    match: {
                        userName: userName
                    }
                }).exec();
                let i;
                for (i = 0; i < empList.length; i++)
                    if (empList[i].details !== null) {
                        await Employee.remove({ _id: empList[i]._id }).exec();
                        await Person.remove({ _id: empList[i].details._id }).exec();
                        break;
                    }
            }

            if (futurePermission != 'noUser')
            {
                let prs = new Person({
                    _id: mongoose.Types.ObjectId(),
                    name: permittedUser.name,
                    age: permittedUser.age,
                    userName: permittedUser.userName,
                    password: permittedUser.password,
                    permission: futurePermission,
                    isConnected: false,
                    valid: true
                });
                prs.save();
            }
        }
        //setTimeout(function () {
        res.end();
        //},1000);
    }
});

router.post('/addUser', async function (req, res) {
    let type = req.body['type'];
    if (type === 'managment' || type === 'employee' || type === 'customer') {
        let prs = new Person({
            _id: mongoose.Types.ObjectId(),
            name: req.body['name'],
            age: req.body['age'],
            userName: req.body['userName'],
            password: req.body['password'],
            permission: type,
            isConnected: false,
            valid: true
        });
        prs.save();
    }
    else if (type === 'branch') {
        let branch = new Branch({
            _id: mongoose.Types.ObjectId(),
            id: req.body['id'],
            address: req.body['address'],
            telephone: req.body['telephone'],
            image: 'images/branches/newBranch.jpg',
            valid: true
        });
        branch.save();
    }

    setTimeout(function () {
        res.end();
    }, 1000);
});

router.get('/deleteUser', async function (req, res) {
    let userName = req.query["userToUpdate"];
    let user = await Person.findOne({ userName: userName }).exec();
    if (user) {
        let permission = user.permission;
        if (permission == 'managment') {
            let mngList = await Managment.find().populate({
                path: 'details',
                match: {
                    userName: userName
                }
            }).exec();
            let i;
            for (i = 0; i < mngList.length; i++)
                if (mngList[i].details !== null) {
                    await Managment.remove({ _id: mngList[i]._id }).exec();
                    await Person.remove({ _id: mngList[i].details._id }).exec();
                    break;
                }
        }
        else if (permission == 'employee') {
            let empList = await Employee.find().populate({
                path: 'details',
                match: {
                    userName: userName
                }
            }).exec();
            let i;
            for (i = 0; i < empList.length; i++)
                if (empList[i].details !== null) {
                    await Employee.remove({ _id: empList[i]._id }).exec();
                    await Person.remove({ _id: empList[i].details._id }).exec();
                    break;
                }
        }
        else if (permission == 'customer') {
            let cstList = await Customer.find().populate({
                path: 'details',
                match: {
                    userName: userName
                }
            }).exec();
            let i;
            for (i = 0; i < cstList.length; i++)
                if (cstList[i].details !== null) {
                    await Customer.remove({ _id: cstList[i]._id }).exec();
                    await Person.remove({ _id: cstList[i].details._id }).exec();
                    break;
                }
        }
    }
    res.end();
});

router.post('/updateBranch', async function (req, res) {
    //console.log('before update: ' + await Branch.findOne({ id: req.body['id'] }).exec());
    await Branch.findOneAndUpdate({ _id: req.body['id'] }, {
        $set: {
            address: req.body['address'],
            telephone: req.body['telephone'],
            image: req.body['image'],
            valid: true                
        }
    }, { new: true }, (err, doc) => {});

    //console.log('after update: ' + await Branch.findOne({ id: req.body['id'] }).exec());
    res.end();
});

router.get('/deleteBranch', async function (req, res) {
    await Branch.remove({ _id: req.query["branchId"] }).exec();
    res.end();
});

module.exports = router;

