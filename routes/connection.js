let express = require('express');
let router = express.Router();
let mongoose = require('mongoose');
let Person = require('../models/Person');
let Branch = require('../models/Branch');


router.post('/login?(*)',async function (req, res) {
    let person = await Person.find().exec();
    console.log('-----------------------/n' + person);
    req.on('data', async chunk => {
        let jsonBody = JSON.parse(chunk.toString());
        let permittedUser = await Person.findOne({userName: jsonBody.userName, password: jsonBody.password}).exec();
        if (permittedUser) {
            await Person.findOneAndUpdate({_id: permittedUser._id}, {$set:{isConnected:true}}, {new: true}, async (err, doc) => {
                if (err) { console.log("Something wrong when updating data!");}
                console.log(await Person.findOne({userName: jsonBody.userName, password: jsonBody.password}).exec());
            });
            res.set('User-Type', permittedUser.permission);
            //setTimeout(function () {
            res.end();
            //},1000);
        }
        else
        //setTimeout(function () {
            res.status(200).send('not permited user');
        //}, 1000);
    });
});

router.get('/logout',async function (req, res) {
    let branches = await Branch.find({valid: true}).exec();
    let userName = req.query["userName"];
    await Person.findOneAndUpdate({userName: userName}, {$set:{isConnected:false}}, {new: true}, async (err, doc) => {
        if (err) { console.log("Something wrong when logout!");}
    });
    //setTimeout(function () {
    res.render('partials/aboutBody', { branches: branches, UserType: 'noUser' });
    //}, 1000);
});

router.get('/header?:userType', function (req, res) {
    //setTimeout(function () {
    res.render('partials/header', { UserType: req.query['userType'], UserName: req.query['userName'] });
    //}, 1000);
});

module.exports = router;
