let express = require('express');
let router = express.Router();
let Person = require('../models/Person');
let Customer = require('../models/Customer');
let Employee = require('../models/Employee');
let Managment = require('../models/Managment');
let Branch = require('../models/Branch');
let Catalog = require('../models/FlowerCatalog');


router.get('/', async function (req, res) {

    let userType = isConnectedUserName(req.query["userName"]);
    let fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    let branches = await Branch.find({valid: true}).exec();
    //setTimeout(function () {
    res.render('pages/about', { branches: branches, UserType: userType });
    //}, 1000);
});

router.get('/contact', function (req, res) {
    //setTimeout(function () {
    res.render('pages/contact');
    //}, 1000);
});

router.get('/about', async function (req, res) {
    let branches = await Branch.find({valid: true}).exec();
    //setTimeout(function () {
    res.render('partials/aboutBody', { branches: branches });
    //}, 1000);
});

router.get('/catalog', async function (req, res) {
    let catalog = await Catalog.find().exec();
    //setTimeout(function () {
    res.render('pages/catalog', { catalog: catalog });
    //}, 1000);
});

module.exports = router;

function isConnectedUserName(usrName) {
    let person = Person.find({ userName: usrName });
    if (person === undefined)
        return 'no-User';
    let permission = person.permission;
    return permission;
}