let createError = require('http-errors');
let express = require('express');
let path = require('path');
let ejs = require('ejs');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
let favicon = require('serve-favicon');
let fs = require('fs');

let mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/myDB', { useNewUrlParser: true });
let db = mongoose.createConnection();

// ruoters
let mngRouter = require('./routes/managment');
let basicRouter = require('./routes/basic');
let connectionRouter = require('./routes/connection');
let customersRouter = require('./routes/customers')
let dbRouter = require('./routes/database');

let app = express();

//app.use(favicon(path.join('public','images','logo.ico')));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/mng', mngRouter);
app.use('/db', dbRouter);
app.use('/', basicRouter);
app.use('/customer', customersRouter);
app.use('/connection', connectionRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;


//comments
//1. delete branch at MANAGEMENT-BRANCH is not working
//2. update branch
//3. when add new branch the telephone doesnt added
//4. add new employee
//5. delete customer as employee
//6.add new flower with image