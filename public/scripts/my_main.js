// globals
var currentUserName;
var currentUserType;
var userToUpdate;
var branchId;
var branchImg;

function changeUser(userType) {
    $("#aboutHeader").load("/connection/header?userType=" + userType +"&userName=" + currentUserName);
}

function validate(psw1, psw2, type) {
    if (psw1 === psw2)
        return true;
    else {
        let p;
        switch (type) {
            case 'managment': p = $('#pswMErr'); break;
            case 'employee': p = $('#pswEmErr'); break;
            case 'customer': p = $('#pswCstErr'); break;
        }
        p.css("visibility", "visible");
        return false;
    }
}

$(document).ready(function () {

    $("#loginBtnn").click(async function (e) {
        e.preventDefault();
        let form = $('#modalForm');
        let _userName = $(form)[0][0].value;
        let _password = $(form)[0][1].value;
        await fetch('/connection/login', {
            method: 'POST',
            body: JSON.stringify({
                userName: _userName,
                password: _password,
            })
        })
            .then(function (response) {
                currentUserType = response.headers.get("User-Type");
                currentUserName = _userName;
                if (currentUserName){
                    //window.location.href = "http://" + window.location.hostname + ':8080/' + "?userName=" + currentUserName;
                    changeUser(currentUserType);
                    //$("#aboutHeader").load("/connection/header?userName=" + currentUserName + "&userType=" + currentUserType);
                }
            })
            .catch(function (error) {
                    console.log('Request failure: ', error);
            });
    });
    
    $("#logoutBtn").click(function (e) {
        e.preventDefault();
        let page = $("#pagein");
        page.load("/connection/logout?userName=" + currentUserName);
        changeUser('noUser');
        currentUserName = '';
        $.getScript("scripts/main.js");
        //window.location.href = "http://" +window.location.hostname + ':8080';
    });
  
});

$(document).on("click", "#updateEmployeePrm", function (e) {
    e.preventDefault();
    console.log(currentUserName);
    let form = $('#employeePermissionModal');
    let userName = form[0][0].value;
    let _permission = form[0][1].value;
    let data = { permission: _permission, userName: userName };
        $.ajax({
            type: "POST",
            url: "/db/updateUser?userName=" + currentUserName,
            data: JSON.stringify(data),
            contentType: "application/json",
            beforeSend: function () {
                $('#waitingModal').show();
            },
            error: function (xhr, status, message) {
                xhr.abort();
                $('#errorModal').show();
            },
            complete: function () {
                //$("#userMngLink").click();
                $('#waitingModal').hide();
            },
            success: function (response) {
                rqsSecced = true;
            },
            timeout: 5000
        });
});

$(document).on("click", "#updateBranchBtn", function (e) {
    e.preventDefault();
    let form = $('#updateModalForm');
    let data = {
        id: form[0][0].value,
        address: form[0][1].value,
        telephone: form[0][2].value,
        image: branchImg
    };
    $.ajax({
        type: "POST",
        url: "/db/updateBranch?userName=" + currentUserName,
        data: JSON.stringify(data),
        contentType: "application/json",
        beforeSend: function () {
            $('#waitingModal').show();
        },
        error: function (xhr, status, message) {
            //xhr.abort();
            //$('#errorModal').show();
        },
        complete: function (response) {
            // $("#branchMngLink").click();
            $('#waitingModal').hide();
        },
        timeout: 5000
    });
});

$(document).on("click", ".delete", function (e) {
    e.preventDefault();
    $.ajax({
        type: "GET",
        url: "/db/deleteUser?userName=" + currentUserName,
        data: { userToUpdate: userToUpdate },
        beforeSend: function () {
            $('#waitingModal').show();
        },
        complete: function () {
            $("#userMngLink").click();
            $('#waitingModal').hide();
        },
        success: function (response) {
            rqsSecced = true;
        }
    });
});

$(document).on("click", "#deleteBranchBtn", function (e) {
    e.preventDefault();
    $.ajax({
        type: "GET",
        url: "/db/deleteBranch?userName=" + currentUserName,
        data: { branchId: branchId },
        beforeSend: function () {
            $('#waitingModal').show();
        },
        complete: function () {
            $("#branchMngLink").click();
            $('#waitingModal').hide();
        },
        success: function (response) {
            rqsSecced = true;
        }
    });
});

$(document).on("click", ".add", function (e) {
    e.preventDefault();
    targeId = this.id;
    let form, psw1, psw2, isValidPsw,url, signUp=false;
    if (targeId == 'signUp')
    {
        signUp = true;
        targeId = 'addCst';
        url = "/connection/signUp?userName=";
    }
    else
        url = "/db/addUser?userName=";
    switch (targeId) {
        case ('addMng'):
            form = $('#newMng');
            psw1 = form[0][2].value;
            psw2 = form[0][3].value;
            isValidPsw = validate(psw1, psw2, 'managment');
            data = {
                type: 'managment',
                name: form[0][0].value,
                userName: form[0][1].value,
                password: psw1
            }
            break;
        case ('addEmp'):
            form = $('#newEmp');
            psw1 = form[0][2].value;
            psw2 = form[0][3].value;
            isValidPsw = validate(psw1, psw2, 'employee');
            data = {
                type: 'employee',
                name: form[0][0].value,
                userName: form[0][1].value,
                password: psw1
            }
            break;
        case ('addCst'):
            form = $('#newCst');
            psw1 = form[0][2].value;
            psw2 = form[0][3].value;
            isValidPsw = validate(psw1, psw2, 'customer');
            data = {
                type: 'customer',
                name: form[0][0].value,
                userName: form[0][1].value,
                password: psw1
            };
            break;
        case ('addBrn'):
            form = $('#newBrn');
            isValidPsw = true;
            data = {
                type: 'branch',
                id: form[0][0].value,
                address: form[0][1].value,
                telephone: form[0][2].value
            };
            // console.log(form[0][0].value,form[0][1].value,form[0][2].value);
            break;
    }
    if (isValidPsw) {
        $.ajax({
            type: "POST",
            url: url + currentUserName,
            data: JSON.stringify(data),
            contentType: "application/json",
            beforeSend: function () {
                $('#waitingModal').show();
            },
            error: function (xhr, status, message) {
                xhr.abort();
                $('#errorModal').show();
            },
            success: function (response) {
                if (targeId == 'addBrn')
                    $("#branchMngLink").click();
                else if (signUp)
                {
                    currentUserName = form[0][1].value;
                    changeUser('customer');
                }
                else
                    $("#refresh").click();
                $('#waitingModal').hide();
            },
            timeout: 5000
        });
    }
    else {

        ('Cannot add a user, please try again');
    }
});

$(document).on("click", "#addFlr",async function (e) {
    e.preventDefault();
    let form = $('#newFlr');
    data = {
                flrName: form[0][0].value,
                flrColor: form[0][1].value,
                flrPrice: form[0][2].value,
                flrImg: form[0][3].value
            };
     console.log(data);
     console.log(data[flrImg]);
    // let formData = new FormData(form[0]);
    // alert(formData);
   // let path = form[0][3].value;
   // alert(path);
    // let name = path.slice(12);
    // alert(name);
    // if (name.slice(-3) === 'jpg')
    //     name = name.slice(0, name.length-4);
    // else if (name.slice(-4) === 'jpeg')
    //     name = name.slice(0, name.length-5);
    //
    // alert(name);
    //FormData.append('imageName', name);
    console.log(currentUserName);
    $.ajax({
            type: "POST",
            url: "/db/addFlower?userName=" + currentUserName,
            data: JSON.stringify(data),
            contentType: "application/json",
            beforeSend: function () {
                $('#waitingModal').show();
            },
            error: function (xhr, status, message) {
                xhr.abort();
                $('#errorModal').show();
            },
            success: function (response) {
                console.log('succeed');
                $("#catalogMngLink").click();
                $('#waitingModal').hide();
            },
            timeout: 5000
    });
});

$(document).on('show.bs.modal', '#updateModal', function (event) {
    let button = $(event.relatedTarget); 
    let userName = button.data('name'); 
    let modal = $(this);
    modal.find('.modal-title').text('Updating ' + userName + ' permissions');
    modal.find('.modal-body input').val(userName);
})

$(document).on('show.bs.modal', '#deleteModal', function (event) {
    let button = $(event.relatedTarget);
    userToUpdate = button.data('username');
    let name = button.data('name');
    let modal = $(this);
    modal.find('.modal-title').text('Deleting ' + name);
    modal.find('.modal-body').text('Are you sure do you want to delete ' + name + '?');    
})

$(document).on('show.bs.modal', '#deleteBranchModal', function (event) {
    let button = $(event.relatedTarget);
    branchId = button.data('id');
    let modal = $(this);
    modal.find('.modal-title').text('Deleting branch');
    modal.find('.modal-body').text('Are you sure do you want to delete ' + ' branch?');
})

$(document).on('show.bs.modal', '#updateBranchModal', function (event) {
    let button = $(event.relatedTarget);
    let id = button.data('id');
    let address = button.data('address');
    let phone = button.data('phone');
    branchImg = button.data('image');
    let modal = $(this);
    modal.find('.modal-title').text('Updating ' + 'branch');
    modal.find('#branchId').val(id);
    modal.find('#address').val(address);
    modal.find('#telephone').val(phone);
})

$(document).on('click', '#refresh', function (e) {
    if (currentUserType == 'managment')
        $("#userMngLink").click();   
    else if (currentUserType == 'employee')
        $("#userMngLinkEmp").click();   
    
})

$(document).on('click', '#refreshCatalog', function (e) {
    $("#catalogMngLink").click();
})

$(document).on('click', '#refreshBrn', function (e) {
    $("#branchMngLink").click();
})

$(document).on('focus', '.psw', function (e) {
    e.preventDefault();
    let p = $('#pswErr');
    p.css("visibility", "hidden");
})  

//----------- header manegment -------------------------

$(document).on("click", "#contactLink", function () {
    let page = $("#pagein");
    page.load("/contact?userName=" + currentUserName);
    changeLink($(this));
    $.getScript("scripts/main.js");
});

$(document).on("click", "#aboutLink", function () {
    let page = $("#pagein");
    page.load("/about?userName=" + currentUserName);
    $.getScript("scripts/main.js");
    changeLink($(this));
});

$(document).on("click", "#catalogLink", function () {
    let page = $("#pagein");
    page.load("/catalog?userName=" + currentUserName);
    $.getScript("scripts/main.js");
    changeLink($(this));
});

$(document).on("click", "#userMngLink", function () {
    let page = $("#pagein");
    page.load("/mng/user?userName=" + currentUserName);
    setTimeout(function () { $.getScript("scripts/main.js");},300);
    changeLink($(this));
});

$(document).on("click", "#userMngLinkEmp", function () {
    let page = $("#pagein");
    page.load("/mng/userEmp?userName=" + currentUserName);
    $.getScript("scripts/main.js");
    //this.css('color', '#cabcab');
    changeLink($(this));
});

$(document).on("click", "#branchMngLink", function () {
    let page = $("#pagein");
    page.load("/mng/branch?userName=" + currentUserName);
    $.getScript("scripts/main.js");
    changeLink($(this));
});

$(document).on("click", "#catalogLink", function () {
    let page = $("#pagein");
    page.load("/catalog?userName=" + currentUserName);
    $.getScript("scripts/main.js");
    changeLink($(this));
});

$(document).on("click", "#catalogMngLink", function () {
    let page = $("#pagein");
    page.load("/mng/catalog?userName=" + currentUserName);
    $.getScript("scripts/main.js");
    changeLink($(this));
});

function changeLink(currentLink) {
     $('#contactLink').css('color', '#000000');
     $('#aboutLink').css('color', '#000000');
     $('#catalogLink').css('color', '#000000');
     $('#userMngLink').css('color', '#000000');
     $('#userMngLinkEmp').css('color', '#000000');
     $('#branchMngLink').css('color', '#000000');
     $('#catalogLink').css('color', '#000000');
     $(currentLink).css('color', '#cabcab');
}






