const debug = require("debug")("mongo:model-manager");
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let spplierSchema = new Schema({
    details: {type: mongoose.Schema.Types.ObjectId, ref:'Person'},
    companyName: String});


let Supplier = mongoose.model('Supplier', spplierSchema);
module.exports = Supplier;