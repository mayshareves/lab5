let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let empSchema = new Schema({
    details: {
        type: Schema.Types.ObjectId,
        ref:'Person'}});

let Employee = mongoose.model('Employee', empSchema);

// Employee.statics('addEmployee', function (name, age, userName, password , callback) {
//     let prs = new Person({
//         _id: new mongoose.Types.ObjectId(),
//         name: name,
//         userName: userName,
//         password: password,
//         permission: 'employee',
//         isConnected: false,
//         valid: true
//     });
//
//     prs.save(function (err) {
//         if (err) return handleError(err);
//         let employee = new Employee({
//             age: age,
//             details: prs._id
//         });
//         employee.save(function (err) {
//             if (err) return handleError(err);
//         });
//     })
// });
// let empSchema = new Schema({name: String,
//     age: Number,
//     userName: String,
//     password: String,
//     permission: Number,//permmissionEnum,
//     isConnected: Boolean,
//     valid: Boolean});
// let Employee = mongoose.model('Employee', empSchema);

module.exports = Employee;