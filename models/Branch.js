let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let branchSchema = new Schema({
    //_id: Schema.Types.ObjectId,
    //id: String,
    //name: String,
    address: String,
    telephone: String,
    image: String,
    valid: Boolean});

let Branch = mongoose.model('Branch', branchSchema);

//Initial Branches
// let branch = new Branch({
//     //_id: 1,
//     address: 'Miami',
//     telephone: '854620314',
//     image: 'images/branches/telAviv.jpeg',
//     valid: true});
//  branch.save();
//
// branch = new Branch({
//     //_id: 1,
//     address: 'Brooklin',
//     telephone: '542698745',
//     image: 'images/branches/ramatGan.jpg',
//     valid: true});
// branch.save();

//Branch.deleteMany({ valid: true }, function (err) {});

module.exports = Branch;

