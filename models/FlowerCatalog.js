let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let flowerSchema = new Schema({
    name: String,
    color: String,
    image: String,
    price: Number,
    valid: Boolean});

let Flower = mongoose.model('Flower', flowerSchema);

//Flower.deleteMany({ valid: true }, function (err) {});
// Initial Flowers
// let flower;
// flower = new Flower({
//     name: "roses",
//     color: "red",
//     image: "images/roses.jpg",
//     price: "150",
//     valid: true
// });
// flower.save();
//
// flower = new Flower({
//     name: "jasminet",
//     color: "white",
//     image: "images/jasmine.jpg",
//     price: "100",
//     valid: true
// });
// flower.save();
//
// flower = new Flower({
//     name: "sagol",
//     color: "purple",
//     image: "images/sagol.jpg",
//     price: "120",
//     valid: true
// });
// flower.save();

module.exports = Flower;

