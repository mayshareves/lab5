const debug = require("debug")("mongo:model-person");
let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let Managment = require('./Managment');
let Employee = require('./Employee');
let Customer = require('./Customer');
let Supplier = require('./Supplier');

let PersonSchema = new Schema({
    _id: Schema.Types.ObjectId,
    name: String,
    userName: String,
    password: String,
    permission: String, //permmissionEnum,
    isConnected: Boolean,
    valid: Boolean});

var companyName = '';

PersonSchema.pre('save',(async function(next){
    switch (this.permission) {
        case 'managment':
            let manager = new Managment({
                details: this._id
            });
            manager.save();
            break;
        case 'employee':
            let employee = new Employee({
                details: this._id
            });
            employee.save();
            break;
        case 'customer':
            let customer = new Customer({
                details: this._id
            });
            customer.save();
            break;
        case 'supplier':
            let supplier = new Supplier({
                details: this._id,
                companyName: companyName
            });
            supplier.save();
            break;
        default:
            break;
    }
    next();
}));

debug("Person model was created");
let Person = mongoose.model('Person', PersonSchema);

//Person.deleteMany({}, function (err) { });
//Supplier.deleteMany({}, function (err) { });
//Employee.deleteMany({}, function(err){});
//Managment.deleteMany({}, function(err){});
//Customer.deleteMany({}, function (err) { });

// let prs;
// prs = new Person({
//    _id: mongoose.Types.ObjectId(),
//    name: "Yaniv",
//    userName: "Yan",
//    password: "0123",
//    permission: "managment",
//    isConnected: false,
//    valid: true
// });
// prs.save();
//
// prs = new Person({
//     _id: mongoose.Types.ObjectId(),
//     name: "Ran",
//     userName: "rani",
//     password: "9090",
//     permission: "supplier",
//     isConnected: false,
//     valid: true
// });
// companyName = 'Rani Flowers';
// prs.save();
//
//  prs = new Person({
//     _id: mongoose.Types.ObjectId(),
//     name: "Doron",
//     userName: "Dor",
//     password: "5231",
//     permission: "employee",
//     isConnected: false,
//     valid: true
//  });
//  prs.save();
//
//  prs = new Person({
//     _id: mongoose.Types.ObjectId(),
//     name: "Liron",
//     userName: "Lir",
//     password: "5157",
//     permission: "employee",
//     isConnected: false,
//     valid: true
//  });
//  prs.save();
//  prs = new Person({
//     _id: mongoose.Types.ObjectId(),
//     name: "Sara",
//     userName: "Sara",
//     password: "5624",
//     permission: "customer",
//     isConnected: false,
//     valid: true
//  });
//  prs.save();
//
// prs = new Person({
//     _id: mongoose.Types.ObjectId(),
//     name: "maysher",
//     userName: "may",
//     password: "1998",
//     permission: "customer",
//     isConnected: false,
//     valid: true
// });
// prs.save();
//
// prs = new Person({
//     _id: mongoose.Types.ObjectId(),
//     name: "shira",
//     userName: "shira",
//     password: "1111",
//     permission: "customer",
//     isConnected: false,
//     valid: true
// });
// prs.save();
//
// prs = new Person({
//     _id: mongoose.Types.ObjectId(),
//     name: "Bracha",
//     userName: "Br",
//     password: "5031",
//     permission: "customer",
//     isConnected: false,
//     valid: true
// });
// prs.save();

module.exports = Person;