const debug = require("debug")("mongo:model-manager");
let mongoose = require('mongoose');
let Schema = mongoose.Schema;
//let Person = require('./Person');

let mngSchema = new Schema({
    details: {type: mongoose.Schema.Types.ObjectId, ref:'Person'},
    });

// mngSchema.statics('addMng', function (name, age, experience, userName, password , callback) {
//     let prs = new Person({
//         _id: new mongoose.Types.ObjectId(),
//         name: name,
//         userName: userName,
//         password: password,
//         permission: 'managment',
//         isConnected: false,
//         valid: true
//     });
//
//     prs.save(function (err) {
//         if (err) return handleError(err);
//         let mng = new Managment({
//             age: age,
//             yearsOfExperience: experience,
//             details: prs._id
//         });
//         mng.save(function (err) {
//             if (err) return handleError(err);
//         });
//     })
// });

let Managment = mongoose.model('Managment', mngSchema);
module.exports = Managment;